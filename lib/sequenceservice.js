//const mongoose = require('mongoose');
const configuration = require('../json/configuration.json');
const CommonMethods = require('./commonmethods.js');

//mongoose.connect(process.env.MONGOLAB_URI || configuration.mongoURI);
//require('../models/Sequence.model.js')
//var Sequence = mongoose.model('Sequence');

const dbConnect = require('./dbconnect.js');

//dbConnect.Connect();
//var Sequence = dbConnect.GetModel('Sequence');



module.exports = {

    GetInc: function (seqName,Key,callback) {

                    dbConnect.Connect(function(db){
                        var Sequence = dbConnect.GetModel('Sequence');

                        Sequence.findOne({seqname: seqName, key: Key}, function(err, data) {

                            if (err) { 
                                console.log(err);
                                callback(err,{value:-1});
                            }
                            //console.log('i m here');
                            //console.log(data);

                            if (data) {
                            //  console.log('data found');
                                var newvalue = data.value + 1;
                                Sequence.update({seqname: seqName, key: Key},{$set:{value:newvalue}}, function(error, seq) {
                                // console.log(seq);
                                    if (seq && seq.ok == 1) {
                                        callback(err,{value:newvalue});
                                    } else {
                                        callback('error while increment the seq.',{value:-1});
                                    }
                                });
                                
                            }
                            else {
                                var sequence = new Sequence({ 
                                                    seqname: seqName,
                                                    key: Key,
                                                    value: 1
                                                });

                                sequence.save(function(err,data) {
                                    callback(err,{value:1});
                                });
                            
                            }    
                        });
                    });
                  },

    Get:  function (seqName,Key,callback) {
                        
                    dbConnect.Connect(function(db){
                        var Sequence = dbConnect.GetModel('Sequence');

                        Sequence.findOne({seqname: seqName, key: Key}
                                                , function(err, data) {
            
                                                        if (err) { 
                                                            console.log(err);
                                                            callback(err,{value:-1});
                                                        }
                                
                                                        if (data) {
                                
                                                            callback(err,{value:data.value});
                                                        }
                                                        else {
                                                            callback(err,{value:-1});
                                                        }
                        });
                    });
      
                }, 

  

}
