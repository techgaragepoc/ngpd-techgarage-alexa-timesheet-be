const Configuration = require('../json/configuration.json');
const ActivityService = require('./activityservice.js');
const CommonMethods = require('./commonmethods.js');
const dbConnect = require('./dbconnect.js');


function SendResponse(obj,err,intent,userid,activities,callback)
{

      if (!err && activities)
        {
            var spkResponse = '';
            var cardResponse = '';

            switch(intent.toUpperCase()) {
                
               case 'GET':     if (!activities || activities.length == 0) {
                                    spkResponse = "No activity found";
                                    cardResponse = spkResponse;
                                }
                                else {
                                    spkResponse = "Activity details are ";
                                    cardResponse = spkResponse;

                                    for(var i=0;i < activities.length;i++) {
                                        
                                        spkResponse += (activities[i].entryno + "<break time='500ms'/>" + activities[i].description + " for <break time='500ms'/>" + activities[i].hours);
                                        cardResponse += (activities[i].entryno + ". " + activities[i].description + " for " + activities[i].hours);

                                        if (activities[i].frachours && activities[i].frachours != 'undefined') 
                                        {
                                            spkResponse += (" point " + activities[i].frachours);
                                            cardResponse += ("." + activities[i].frachours);
                                        }

                                        spkResponse += " hours <break time='500ms'/>";
                                        cardResponse += " hours; ";
                                    }

                                    spkResponse += " End <break time='500ms'/>";
                                }
                                break;
               
               case 'DEL':      spkResponse = "Activity has been deleted, anything else? <break time='500ms'/>";
                                break;

               case 'ADD':      spkResponse = "Activity has been added, anything else? <break time='500ms'/>";
                                break;

            }

            console.log(spkResponse);
            callback(obj,"ASKWITHCARD", spkResponse, "do you want to add or delete any activity?","Activity",cardResponse);
        }
        else
        {
            console.log(err);

            if (err && err.message) {
                callback(obj,"ASK", err.message + "<break time='3s'/>","","activity",err.message);
            }
            else {
                callback(obj,"ASK", "error occured while executing the request, please try again <break time='3s'/>","","","");
            }
            
        }
}

module.exports = {

Initialize: function(self,cb) {

    dbConnect.Connect(function(db){
        cb(dbConnect.GetModel('Activity'));
    });
},

ProcessIntent: function(self,intent,activity,cb)
                        {

                            console.log('activity => ' + activity);
                            if (!activity)
                            {
                                //self.emit(":ask", "Sorry, I could not get the activity details properly");
                                var msg = "Sorry, I could not get the activity details properly";
                                cb(self,"ASK",msg,msg,"","");
                            }
                            else {
                                var userid = activity.userId;
                                switch(intent.toUpperCase()) {

                                    case 'ADD': ActivityService.Add(userid,activity,function(err,data){
                                                   SendResponse(self,err,intent,userid,activity,function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                                       cb(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
                                                   });
                                                }); 
                                                break;

                                    case 'GET': ActivityService.Get(userid,activity,function(err,data){
                                                    SendResponse(self,err,intent,userid,data,function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                                        cb(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
                                                    });
                                                }); 
                                                break;                                                
                                    case 'DEL': ActivityService.Delete(userid,activity,function(err,data){
                                                    SendResponse(self,err,intent,userid,activity,function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                                        cb(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
                                                    });
                                                }); 
                                                break;

                                    }   
                                }

                        },

/*
SendResponse: function(err,intent,userid,activities,obj)
                {

                      if (!err && activities)
                        {
                            var spkResponse = '';

                            switch(intent.toUpperCase()) {
                                
                               case 'GET':     if (!activities || activities.length == 0) {
                                                    spkResponse = "No activity found";
                                                }
                                                else {
                                                    spkResponse = "Activity details are ";

                                                    for(var i=0;i < activities.length;i++) {
                                                        
                                                        spkResponse += (activities[i].entryno + "<break time='500ms'/>" + activities[i].description + " for <break time='500ms'/>" + activities[i].hours);

                                                        if (activities[i].frachours && activities[i].frachours != 'undefined') 
                                                        {
                                                            spkResponse += (" point " + activities[i].frachours);
                                                        }
                                                        spkResponse += " hours <break time='500ms'/>";
                                                    }
                                                }
                                                break;
                               
                               case 'DEL':      spkResponse = "Activity has been deleted, anything else? <break time='500ms'/>";
                                                break;

                               case 'ADD':      spkResponse = "Activity has been added, anything else? <break time='500ms'/>";
                                                break;

                            }

                            console.log(spkResponse);
                            obj.emit(":askWithCard", spkResponse, "do you want to add or delete any activity?","activity",spkResponse);
                        }
                        else
                        {
                            console.log(err);

                            if (err && err.message) {
                                obj.emit(":ask", err.message + "<break time='3s'/>");
                            }
                            else {
                                obj.emit(":ask", "error occured while executing the request, please try again <break time='3s'/>");
                            }
                            
                        }
                }
*/

}