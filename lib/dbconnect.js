const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const configuration = require('../json/configuration.json');

var isLocal = false;

if (configuration.environment && configuration.environment.toLowerCase() == 'local') {
    isLocal = true;
    console.log('connected to local environment...');
}

module.exports = {

    Connect: function(callback) {

                    var mongoConnectURI = (process.env.MONGOLAB_URI || configuration.mongoURIlocal);

                    if (!isLocal) {
                        mongoConnectURI = (process.env.MONGOLAB_URI || configuration.mongoURI);
                    }

                    var promise = mongoose.connect(mongoConnectURI, {
                        useMongoClient: true,
                        /* other options */
                      });

                      promise.then(function(db) {
                        // Use `db`, for instance `db.model()`
                            // create a variable that points to the path where all of the models live
                            var models_path = path.join(__dirname, '../models');
                            // read all of the files in the models_path and require (run) each of the javascript files
                            fs.readdirSync(models_path).forEach(function(file) {
                                if (file.indexOf('.js') >= 0) {
                                    // require the file (this runs the model file which registers the schema)
                                    require(models_path + '/' + file);
                                }
                            });


                            callback(db);
                      });  
                    
                        
                       // require('../models/Activity.model.js');
                       // require('../models/Sequence.model.js');
                       // return;
    },

    GetModel: function(model) {
            return mongoose.model(model);
    }
}