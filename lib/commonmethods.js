

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function right(val,padchar,totallen) {
    var str = padchar;

    for(var i=0; i<totallen;i++)
        str += padchar;

    str += val;

    return  str.substr(str.length-totallen,str.length);
}

module.exports = {

  SearchStringInArray: function(str, strArray) {
                            for (var j=0; j<strArray.length; j++) {
                                if (strArray[j].match(str)) return j;
                            }
                            return -1;
                        },


  ReplaceAll:          function(str, find, replace) {
                          return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
                      },

  Right:              function (val,padchar,totallen) {
                        return  right(val,padchar,totallen);
                    },
                    
  GetDateAsString:    function (dt) {
                        var date = new Date(dt);
                        var d = date.getDate();
                        var m = date.getMonth()+1;
                        var y = date.getFullYear();
                        var dateStr = y + '-' + right(m,'0',2) + '-' + right(d,'0',2);
                    
                       // console.log(dateStr);
                       return dateStr;
                    }                      

/*
  GetEmailId:         function (subs,subscribers)
                          {
                            for(var sCtr = 0;sCtr < subscribers.length; sCtr++)
                            {
                                if (subs.toUpperCase() == subscribers[sCtr].name.toUpperCase())
                                {
                                   return subscribers[sCtr].email;
                                }
                            }

                            return '';
                          },     

GetTitle:              function(titles)
                        {
                            var title = '';

                            if (titles) {
                                for(var i=0;i<titles.length;i++)
                                    {
                                        title += (titles[i]);
                                        if (i < (titles.length-1))
                                        title += '|';
                                    }
                            }
                            return title;    
                        },     
                        
GetUserByUserId:        function (userid,subscribers)
                        {
                            for(var sCtr = 0;sCtr < subscribers.length; sCtr++)
                            {
                                if (userid == subscribers[sCtr].alexaUserId)
                                {
                                    return subscribers[sCtr];
                                }
                            }

                            return null;

                        },
 */

}
