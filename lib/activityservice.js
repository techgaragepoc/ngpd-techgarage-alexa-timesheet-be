
const configuration = require('../json/configuration.json');
const CommonMethods = require('./commonmethods.js');
const SequenceService = require('./sequenceservice.js');

const EntryNoSeqName = 'entrySeq';
const dbConnect = require('./dbconnect.js');

var deletionlist = [];

//function markfordeletion('MARKED',userId,data[i].activityon, data[i].entryno) {
function markfordeletion(operation,userId,activityon, entryno,cb) {
  
    if (operation.toUpperCase() == 'MARKED') {
        deletionlist.push({userid: userId, activityon: activityon, entryno: entryno,removed: false});
    }

    if (operation.toUpperCase() == 'REMOVED') {

            for(var iCtr = 0; iCtr < deletionlist.length; iCtr++) {
                if (deletionlist.userid == userId && deletionlist.activityon == activityon
                        && deletionlist.entryno  == entryno)
                        {
                            deletionlist[iCtr] = {userid: userId, activityon: activityon, entryno: entryno,removed: true};
                        }
            }
        
            for(var iCtr = 0; iCtr < deletionlist.length; iCtr++) {
                if (deletionlist.removed == false) {
                    return;
                    break;
                }
            }

            cb(null,{status: "deleted"});
       }
    }
   

module.exports = {

    Add: function (userId,activity,callback) {

                            if (!activity) { 
                                callback({error: 'activity object is null'},null); 
                                return; 
                              }
                             else {

                                var activityOn = '';
                                
                                if (activity.activityOn)
                                    activityOn = CommonMethods.GetDateAsString(activity.activityOn);
                                else
                                    activityOn = CommonMethods.GetDateAsString(new Date());

                                console.log('Add operation for date => ' + activityOn);
                                var activity_key = activity.userId + '|' + activityOn;

                                dbConnect.Connect(function(db){
                                    var Activity = dbConnect.GetModel('Activity');

                                    SequenceService.GetInc(EntryNoSeqName,activity_key,function(err,seq){
                                        
                                            if (err) { callback(err,null); }
    
                                            if (seq) {
    
                                            var activityToRecord = new Activity({ 
                                                userid: activity.userId,
                                                activityon: activityOn,
                                                entryno: seq.value,
                                                description: activity.description,
                                                hours: activity.hours,
                                                frachours: activity.fracHours,
                                            });
            
                                            activityToRecord.save(function(err, act) {
                                                if (err) {
                                                    console.log('error while saving activity: ' + err);
                                                    callback(err,null);
                                                } else {
                                                    console.log('activity saved');
                                                    callback(err,act);
                                                }
                                            });
    
                                            }
                                       });
                                });
                              }
                            },

    Get:  function (userId,activity,callback) {

                        var activityOn = '';

                        if (activity.activityOn)
                             activityOn = CommonMethods.GetDateAsString(activity.activityOn);
                        else
                             activityOn = CommonMethods.GetDateAsString(new Date());     

                        console.log('Get operation for date => ' + activityOn);

                        dbConnect.Connect(function(db){
                            var Activity = dbConnect.GetModel('Activity');

                            Activity.find({userid: userId, activityon: activityOn},function(err,data){
                                if (err) {
                                  console.log('error while fetching activity: ' + err);
                                  callback(err,null);
                                } else {
                                    console.log('activities retrieved - ' + data);
                                    callback(err,data); 
                                }
  
                            });
                            
                        });

                      
                    }, 

    Delete:   function(userId,activity,callback)
                  {
                    var allEntriesOftheDay = false;
                    var activityOn = '';

                      //if entry no is not mention then all entries to be considered for that day  
                      if (activity.entryNo ==null || activity.entryNo == 'undefined' || activity.entryNo == 0 ) 
                      {
                        allEntriesOftheDay = true; 
                      }

                      if (activity.activityOn)
                            activityOn = CommonMethods.GetDateAsString(activity.activityOn);
                      else
                            activityOn = CommonMethods.GetDateAsString(new Date());       

                       console.log('delete for date = ' + activityOn + ' and entryno = ' + activity.entryNo);

                       dbConnect.Connect(function(db){

                                    var Activity = dbConnect.GetModel('Activity');

                                    Activity.find({userid: userId,activityon: activityOn},function(err,data){
                                        if (err) {
                                        console.log('error while fetching activity: ' + err);
                                        callback(err,null);
                                        } else {
                                            console.log('activities retrived - ' + data);

                                            if (allEntriesOftheDay) {
                                                Activity.remove({userid: userId,activityon: activityOn},function(error,removed){
                                                    callback(null,removed);
                                                });
                                            }
                                            else {

                                                for(var i=0;i<data.length;i++) {
                                                    
                                                        if (activity.entryNo == data[i].entryno) {
        
                                                            //markfordeletion('MARKED',userId,activityOn, data[i].entryno,callback);
        
                                                            Activity.remove({userid: userId,activityon: activityOn, entryno: data[i].entryno},function(error,removed){
                                                                   // console.log(removed);
                                                                   // markfordeletion('REMOVED',userId,activityOn, data[i].entryno,callback);
                                                                   callback(null,removed);
                                                            });
                                                        }
                                                    }

                                            }
                                            
                                           
            
                                           // if (data) {callback(err,data); }
                                           // else {callback({message: 'No activity found'},null); }

                                            
                                        }
            
                                    });
                       });


                  }

}
