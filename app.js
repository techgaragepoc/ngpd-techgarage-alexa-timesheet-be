var express = require("express");
var alexa = require("alexa-app");
var promise = require("promise");

const responses = require('./json/commonResponses.json');
const configuration = require('./json/configuration.json');
const CommonMethods = require('./lib/commonmethods.js');
const MainService = require('./lib/mainservice.js');

const APP_ID = configuration.appId;

var PORT = process.env.port || 8080;
var app = express();
var activityModel = null;

// ALWAYS setup the alexa app and attach it to express before anything else.
var alexaApp = new alexa.app("alexaWS");

alexaApp.express({
  expressApp: app,

  // verifies requests come from amazon alexa. Must be enabled for production.
  // You can disable this if you're running a dev environment and want to POST
  // things to test behavior. enabled by default.
  checkCert: false,

  // sets up a GET route when set to true. This is handy for testing in
  // development, but not recommended for production. disabled by default
  debug: true
});

// now POST calls to /test in express will be handled by the app.request() function

// from here on you can setup any other express routes or middlewares as normal
app.set("view engine", "ejs");


function initiateSession(request)
{

    var activity = {};
    activity["userId"] = '';
    activity["description"] = request.slot("ACTIVITY");
    activity["activityOn"] = request.slot("TIMESHEETDAY");
    activity["hours"] = request.slot("HOUR");
    activity["fracHours"] = request.slot("FRAC_HOURS");
    activity["entryNo"] = request.slot("ENTRYNO");

    //MainService.Initialize(request,function(activitymdl) {
    //    activityModel = activitymdl;
    //});

    // check if you can use session (read or write)
    if (request.hasSession())
    {
        // get the session object
        var session = request.getSession();
        var sessionDetail = session.details;
        //session.set(String attributeName, String attributeValue)

        // return the value of a session variable
        console.log(sessionDetail);

        var reqAppId = sessionDetail.application.applicationId;
        var alexaUserId = sessionDetail.user.userId;

        if (reqAppId != configuration.appId) {
            console.log('Invalid Alexa skill trying to call... ' + reqAppId);
            return null;
        }

        activity["userId"] = alexaUserId;
        var currUserId = session.get('userId');

        console.log('user id identified as => ' + alexaUserId);
        console.log('current user => '+ currUserId);
   
        if (currUserId == null || currUserId == 'undefined' 
                || currUserId == '')
            {

                session.set('timesheet_date',CommonMethods.GetDateAsString(new Date()));
                session.set('intent_to_exec','');
                session.set('userId',alexaUserId);
            }
   
        var todaydate = new Date();
        var timesheetdate = activity.activityOn;

        if (timesheetdate !=null && timesheetdate != 'undefined')
                session.set('timesheet_date',timesheetdate);
        else 
                session.set('timesheet_date',CommonMethods.GetDateAsString(todaydate));
    }

   return  activity;   
}


function getCard(cardTitle,cardContent) {
    return {
        "type": "Simple",
        "title":cardTitle,
        "content": cardContent
    };
}

function responseToAlexa(response,responsetype,spkresponse,prompt,cardtitle,cardtext) {
       
        var cardObj = getCard(cardtitle,cardtext);
        console.log(cardObj);

        switch (responsetype.toUpperCase()) {
            case 'TELL': return response.say(spkresponse).send(); break;
            case 'ASK': return response.say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
            case 'ASKWITHCARD': return response.card(cardtitle,cardtext).say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
        }
}
    
function CallIntentAction(obj,request,intentcode,callback) {
    
                var requestForActivity = initiateSession(request);
        
                if (requestForActivity == null || requestForActivity == 'undefined') {
                    //responseToAlexa(response,'TELL',"Invalid Alexa skill trying to call...","","","");
                    callback("TELL", "Invalid Alexa skill trying to call the service...",
                            "Invalid Alexa skill trying to call the service...",
                            "Activity Recorder","Invalid Alexa skill trying to call the service...");
                }
                else {
               
                    switch (intentcode.toUpperCase()) {
                      /*  case 'LAUNCH': callback('ASK',responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt,"",""); break;
                        case 'HELP': callback('ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"",""); break;
                        case 'START' : callback('ASK', responses["ActivityServiceStarted"].ask, responses["ActivityServiceStarted"].reprompt,"",""); break;
                        case 'STOP' :  callback('TELL',responses["ActivityServiceStopped"].tell, "","",""); break;
                        */
                        case 'ADD':
                        case 'GET':
                        case 'DEL':  MainService.ProcessIntent(obj,intentcode,requestForActivity, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                        console.log(responsetype + ' : ' + spkresponse + ' $ ' + prompt);
                                        callback(responsetype,spkresponse,prompt,cardtitle,cardtext);
                                    }); 
                                    break;  
                    }
                }
     }

alexaApp.error = function(exception, request, response) {
    console.log(exception);
    response.say("Sorry, something bad happened");
};

alexaApp.pre = function(request, response, type) {
    if (request.applicationId != configuration.appId) {
      // fail ungracefully
      //throw "Invalid applicationId";
      return response.fail("Invalid applicationId");
    }
  };

alexaApp.post = function(request, response, type, exception) {
    console.log('sending repsonse...');
    console.log(response);

    if (exception) {
      // always turn an exception into a successful response
      console.log('exception while sending response...');
      console.log(exception);
       return response.clear().say("An error occured: " + exception).send();
    }
  };  

alexaApp.launch(function(request, response) {
    responseToAlexa(response,'ASK',responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt,"","");
});

alexaApp.intent("AMAZON.HelpIntent", {},function (request, response) {
    responseToAlexa(response,'ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"","");
});

alexaApp.intent("AMAZON.StopIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ActivityServiceStopped"].tell, "","","");
});

alexaApp.intent("AMAZON.CancelIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ActivityServiceStopped"].tell, "","","");
});

alexaApp.intent("SessionEndedRequest", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ActivityServiceStopped"].tell, "","","");
});

alexaApp.sessionEnded(function(request, response) {
    responseToAlexa(response,'TELL',responses["ActivityServiceStopped"].tell, "","","");
  });

alexaApp.intent("StartActivityRecordService", {}, function(request, response) {
    responseToAlexa(response,'ASK', responses["ActivityServiceStarted"].ask, responses["ActivityServiceStarted"].reprompt,"","");
});

alexaApp.intent("StopActivityRecordService", {}, function(request, response) {
    responseToAlexa(response,'TELL',responses["ActivityServiceStopped"].tell, "","","");
});

alexaApp.intent("AddActivity", {},  function(request, response) {
    var intentAction = new promise(function(resolve,reject) {
        CallIntentAction(this,request,'ADD',function(responsetype,spkresponse,prompt,cardtitle,cardtext){            console.log(responsetype + ' : ' + spkresponse + ' $ ' + prompt);
            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };

            resolve(outputObj);
        })
    }); 

    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });
  });

alexaApp.intent("DeleteActivity", {},  function(request, response) {
    var intentAction = new promise(function(resolve,reject) {
        CallIntentAction(this,request,'DEL',function(responsetype,spkresponse,prompt,cardtitle,cardtext){
            console.log(responsetype + ' : ' + spkresponse + ' $ ' + prompt);
            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };

            resolve(outputObj);
        })
    }); 

    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });
  });

alexaApp.intent("GetRecordedActivities", {},  function(request, response) {
    var intentAction = new promise(function(resolve,reject) {
        CallIntentAction(this,request,'GET',function(responsetype,spkresponse,prompt,cardtitle,cardtext){
            console.log(responsetype + ' : ' + spkresponse + ' $ ' + prompt);
            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };

            resolve(outputObj);
        });
       
    }); 

    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });

});

app.listen(PORT);
console.log("Listening on port " + PORT + ", try http://localhost:" + PORT + "/alexaWS");