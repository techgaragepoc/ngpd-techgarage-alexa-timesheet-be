var Alexa = require('alexa-sdk');

const responses = require('./json/commonResponses.json');
const configuration = require('./json/configuration.json');
const CommonMethods = require('./lib/commonmethods.js');
const MainService = require('./lib/mainservice.js');

const APP_ID = configuration.appId;

var IsUserRecognized = false;
var AfterConfirmation = false;


/**
function populateObjectfromSlotValues(obj) {
    
        var activity =   {      userId: obj.event.session.user.userId,
                           description: obj.event.request.intent.slots.ACTIVITY.value,
                            activityOn: obj.event.request.intent.slots.TIMESHEETDAY.value,
                                 hours: obj.event.request.intent.slots.HOUR.value,
                             fracHours: obj.event.request.intent.slots.FRAC_HOURS.value,
                               entryNo: obj.event.request.intent.slots.ENTRYNO.value
                        };
    
        return activity;            
}

function setSessionValues(obj,timesheetdate)
{
        var todaydate = new Date();

        if (timesheetdate !=null && timesheetdate != 'undefined')
            obj.attributes['timesheet_date'] = timesheetdate;
        else 
            obj.attributes['timesheet_date'] = CommonMethods.GetDateAsString(todaydate);
}

**/

function initiateSession(obj)
 {

    var currUserId = obj.attributes['userId'];

    console.log('user id identified as => ' + obj.event.session.user.userId);
    console.log('current user => '+ currUserId);
    
    if (currUserId == null || currUserId == 'undefined' 
            || currUserId == '')
        {

            obj.attributes['timesheet_date'] = CommonMethods.GetDateAsString(new Date());
            obj.attributes['intent_to_exec'] = '';
            obj.attributes['userId'] = obj.event.session.user.userId;
        }

    
    var activity =   {      userId: obj.event.session.user.userId,
                        description: obj.event.request.intent.slots.ACTIVITY.value,
                            activityOn: obj.event.request.intent.slots.TIMESHEETDAY.value,
                                hours: obj.event.request.intent.slots.HOUR.value,
                            fracHours: obj.event.request.intent.slots.FRAC_HOURS.value,
                            entryNo: obj.event.request.intent.slots.ENTRYNO.value
                };    

    var todaydate = new Date();
    var timesheetdate = activity.activityOn;

    if (timesheetdate !=null && timesheetdate != 'undefined')
        obj.attributes['timesheet_date'] = timesheetdate;
    else 
        obj.attributes['timesheet_date'] = CommonMethods.GetDateAsString(todaydate);

    return  activity;   
 }




 function responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext) {

    switch (responsetype.toUpperCase()) {
        case 'TELL': obj.emit(':tell',spkresponse); break;
        case 'ASK': obj.emit(':ask',spkresponse,prompt); break;
        case 'ASKWITHCARD': obj.emit(':askWithCard',spkresponse,prompt,cardtitle,cardtext); break;
    }
    
    return;     
 }

 function CallIntentAction(obj,intentcode) {

            var requestForActivity = initiateSession(obj);
    
            switch (intentcode.toUpperCase()) {
                case 'LAUNCH': responseToAlexa(this,'ASK',responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt,"",""); break;
                case 'HELP': responseToAlexa(this,'ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"",""); break;
                case 'START' : responseToAlexa(this,'ASK', responses["ActivityServiceStarted"].ask, responses["ActivityServiceStarted"].reprompt,"",""); break;
                case 'STOP' :  responseToAlexa(this,'TELL',responses["ActivityServiceStopped"].tell, "","",""); break;
                default:  MainService.ProcessIntent(obj,intentcode,requestForActivity, function(intentObj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                responseToAlexa(intentObj,responsetype,spkresponse,prompt,cardtitle,cardtext);
                            }); 
                            break;
            }
 }


const handlers = {

    'LaunchRequest': function () {
        CallIntentAction(this,'LAUNCH');
    },
    'AMAZON.HelpIntent': function () {
        CallIntentAction(this,'HELP');
    },
    'AMAZON.StopIntent': function () {
        CallIntentAction(this,'STOP');
    },
    'AMAZON.CancelIntent': function () {
        CallIntentAction(this,'STOP');
    },
    'SessionEndedRequest': function () {
        CallIntentAction(this,'STOP');
    },
    'Unhandled': function () {
        CallIntentAction(this,'HELP');
    },

    'StartActivityRecordService': function () {  //invoke statement: ask Mercer to start profile service
        CallIntentAction(this,'START');
    },
    'StopActivityRecordService': function () {  //invoke statement: ask Mercer to stop profile service
        CallIntentAction(this,'STOP');
    },
    
    'AddActivity': function () {
        CallIntentAction(this,'ADD');
    },
    
    'DeleteActivity': function () {
        CallIntentAction(this,'DEL');
    },

    'GetRecordedActivities': function () {
        CallIntentAction(this,'GET');
    },            

};

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(handlers);
    console.log(event.session.user.userId);
    alexa.execute();
};
