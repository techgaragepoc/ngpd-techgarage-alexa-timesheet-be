
// require mongoose
const mongoose = require('mongoose');

// create the schema
var ApplicationSchema = new mongoose.Schema({

    userid: {
        type: String,
    },
    activityon : {
        type: String,
    },
    entryno : {
        type: Number,
    },
    description : {
        type: String,
    },
    hours : {
        type: String,
    },
    frachours : {
        type: String,
    },
    recordedon : {
        type: Date,
        default: Date.now
    }
})

// register the schema as a model
var Application = mongoose.model('Activity', ApplicationSchema);