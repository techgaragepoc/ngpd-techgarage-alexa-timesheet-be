
// require mongoose
const mongoose = require('mongoose');

// create the schema
var ApplicationSchema = new mongoose.Schema({

    seqname: {
        type: String,
    },
    key : {
        type: String,
    },
    value : {
        type: Number,
    },
    lastupdatedon : {
        type: Date,
        default: Date.now
    }
})

// register the schema as a model
var Application = mongoose.model('Sequence', ApplicationSchema);